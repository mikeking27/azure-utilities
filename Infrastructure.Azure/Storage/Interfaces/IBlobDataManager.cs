﻿namespace Infrastructure.Azure.Interfaces
{
    using System.Threading.Tasks;
    using Shared;

    public interface IBlobDataManager
    {
        ProgressRecorder ProgressRecorder { get; set; }
        Task UploadFileToBlobAsync(string sourceFilePath, BlobProperties properties);
    }
}