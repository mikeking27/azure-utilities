﻿namespace Infrastructure.Azure.Interfaces
{
    using System.Collections.Generic;
    using System.IO;
    using Microsoft.WindowsAzure.Storage.Blob;

    public interface IBlobStorageUtility
    {
        string AddDataToAppendBlockBlob(string blobId, string data);
        string CreateBlockBlob(string blobId, string contentType, Stream data);
        string CreateBlockBlob(string blobId, string contentType, byte[] data);
        string CreateBlockBlob(string blobId, string contentType, string data);
        string CreateBlockBlob(string blobId, string filePath);
        void DeleteBlob(string blobId);
        void DeleteBlobContainer();
        CloudAppendBlob GetAppendBlockBlobReference(string blobId);
        CloudBlockBlob GetBlockBlobReference(string blobId);
        Stream GetBlockBlobDataAsStream(string blobId);
        string GetBlockBlobDataAsString(string blobId);
        IEnumerable<IListBlobItem> ListBlobsInContainer();
    }
}