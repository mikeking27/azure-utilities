﻿namespace Infrastructure.Azure.Interfaces
{
    using System.IO;
    using System.Threading.Tasks;

    public interface IAsyncFileStorageUtility
    {
        Task CreateDirectoryAsync(string directoryName);
        Task DeleteDirectoryAsync(string directoryName);
        Task WriteTextToFileAsync(string directoryName, string fileName, string content);
        Task WriteStreamToFileAsync(string directoryName, string fileName, Stream content);
        Task UploadFileAsync(string directoryName, string fileName, string sourceFilePath);
        Task DeleteFileAsync(string directoryName, string fileName);
        bool DirectoryExists(string directoryName);
        bool FileExists(string directoryName, string fileName);
    }
}