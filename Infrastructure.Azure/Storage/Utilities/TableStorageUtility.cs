﻿namespace Infrastructure.Azure.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces;
    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.RetryPolicies;
    using Microsoft.WindowsAzure.Storage.Table;
    using Shared;

    public class TableStorageUtility<T> : ITableStorageUtility<T> where T : TableEntity, new()
    {
        private readonly CloudTable _cloudTable;

        /// <summary>
        ///     Creates a new TableStorage object
        /// </summary>
        /// <param name="tableName">The name of the table to be managed</param>
        /// <param name="storageConnectionString">
        ///     The connection string pointing to the storage account (this can be local or
        ///     hosted in Windows Azure
        /// </param>
        public TableStorageUtility(string tableName, string storageConnectionString)
        {
            Validate.TableName(tableName, "tableName");
            Validate.String(storageConnectionString, "storageConnectionString");

            var cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            var requestOptions = new TableRequestOptions
            {
                RetryPolicy = new ExponentialRetry(TimeSpan.FromSeconds(1), 3)
            };

            var cloudTableClient = cloudStorageAccount.CreateCloudTableClient();
            cloudTableClient.DefaultRequestOptions = requestOptions;

            _cloudTable = cloudTableClient.GetTableReference(tableName);
            _cloudTable.CreateIfNotExists();
        }

        /// <summary>
        ///     Creates a new entity in the table
        /// </summary>
        /// <param name="entity">The entity to store in the table</param>
        public void CreateEntity(T entity)
        {
            Validate.Null(entity, "entity");
            var insertOperation = TableOperation.Insert(entity);
            _cloudTable.Execute(insertOperation);
        }

        /// <summary>
        ///     Creates new entities in the table using batching
        /// </summary>
        /// <param name="entities">The entities to store in the table</param>
        public void CreateEntities(IEnumerable<T> entities)
        {
            Validate.Null(entities, "entities");
            var batchOperation = new TableBatchOperation();

            foreach (var entity in entities)
                batchOperation.Insert(entity);

            _cloudTable.ExecuteBatch(batchOperation);
        }

        /// <summary>
        ///     Create an entity if it doesn't exist or merges the new values
        ///     to the existing one
        /// </summary>
        /// <param name="entity"></param>
        public void InsertOrUpdate(T entity)
        {
            Validate.Null(entity, "entity");
            var insertOrUpdateOperation = TableOperation.InsertOrMerge(entity);
            _cloudTable.Execute(insertOrUpdateOperation);
        }

        /// <summary>
        ///     Deletes an entities from the table with the specified partitionKey
        /// </summary>
        /// <param name="partitionKey">
        ///     The partition key of the entity to be deleted.
        ///     Note that a partition key can return more than one entity.
        ///     If more than one are returned, the first one is deleted.
        /// </param>
        public void DeleteEntitiesByPartitionKey(string partitionKey)
        {
            Validate.TablePropertyValue(partitionKey, "partitionKey");

            var query =
                new TableQuery<T>()
                    .Where(TableQuery.GenerateFilterCondition(
                        "PartitionKey",
                        QueryComparisons.Equal,
                        partitionKey));

            var results = _cloudTable.ExecuteQuery(query);
            var batchOperation = new TableBatchOperation();
            foreach (var batch in results.Batch(100))
            {
                foreach (var entity in batch)
                    batchOperation.Delete(entity);
                _cloudTable.ExecuteBatch(batchOperation);
                batchOperation = new TableBatchOperation();
            }
        }

        /// <summary>
        ///     Deletes an entities from the table with the specified partitionKey
        /// </summary>
        /// <param name="rowKey">
        ///     The row key of the entities to be deleted.
        ///     Note that a row key can return more than one entity.
        ///     If more than one are returned, the first one is deleted.
        /// </param>
        public void DeleteEntitiesByRowKey(string rowKey)
        {
            Validate.TablePropertyValue(rowKey, "rowKey");

            var query =
                new TableQuery<T>()
                    .Where(TableQuery.GenerateFilterCondition(
                        "RowKey",
                        QueryComparisons.Equal,
                        rowKey));

            var results = _cloudTable.ExecuteQuery(query);
            var batchOperation = new TableBatchOperation();
            foreach (var batch in results.Batch(100))
            {
                foreach (var entity in batch)
                    batchOperation.Delete(entity);
                _cloudTable.ExecuteBatch(batchOperation);
                batchOperation = new TableBatchOperation();
            }
        }

        /// <summary>
        ///     Deletes an entity from the table
        /// </summary>
        /// <param name="partitionKey">The partitionKey of the entity</param>
        /// <param name="rowKey">The row key of the entity to be deleted</param>
        public void DeleteEntity(string partitionKey, string rowKey)
        {
            Validate.TablePropertyValue(rowKey, "rowKey");
            Validate.TablePropertyValue(partitionKey, "partitionKey");

            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            var retrievedResult = _cloudTable.Execute(retrieveOperation);

            if (retrievedResult.Result is T entityToDelete)
            {
                var deleteOperation = TableOperation.Delete(entityToDelete);
                _cloudTable.Execute(deleteOperation);
            }
        }

        /// <summary>
        ///     Delete the table for this particular TableStorage
        /// </summary>
        public void DeleteTable()
        {
            _cloudTable.DeleteIfExists();
        }

        /// <summary>
        ///     Gets an entity from the table
        /// </summary>
        /// <param name="partitionKey">
        ///     The partition key of the entity to be returned.
        /// </param>
        public IEnumerable<T> GetEntitiesByPartitionKey(string partitionKey)
        {
            Validate.TablePropertyValue(partitionKey, "partitionKey");

            var query =
                new TableQuery<T>()
                    .Where(TableQuery.GenerateFilterCondition(
                        "PartitionKey",
                        QueryComparisons.Equal,
                        partitionKey));

            return _cloudTable.ExecuteQuery(query).AsEnumerable();
        }

        /// <summary>
        ///     Gets all entities from the table with the specified rowKey
        /// </summary>
        /// <param name="rowKey">
        ///     The row key of the entities to be returned.
        /// </param>
        public IEnumerable<T> GetEntitiesByRowKey(string rowKey)
        {
            Validate.TablePropertyValue(rowKey, "rowKey");

            var query =
                new TableQuery<T>()
                    .Where(TableQuery.GenerateFilterCondition(
                        "RowKey",
                        QueryComparisons.Equal,
                        rowKey));

            return _cloudTable.ExecuteQuery(query).AsEnumerable();
        }

        /// <summary>
        ///     Gets an entity from the table
        /// </summary>
        /// <param name="partitionKey">
        ///     The partition key of the entity to be returned.
        ///     The partition key and row key will always return a single entity.
        /// </param>
        /// <param name="rowKey">
        ///     The row key of the entity to be returned.
        ///     The partition key and row key will always return a single entity.
        /// </param>
        public T GetEntityByPartitionKeyAndRowKey(string partitionKey, string rowKey)
        {
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);

            var retrievedResult = _cloudTable.Execute(retrieveOperation);

            return retrievedResult.Result as T;
        }
    }
}