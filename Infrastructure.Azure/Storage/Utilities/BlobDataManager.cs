﻿namespace Infrastructure.Azure.Utilities
{
    using System.Threading;
    using System.Threading.Tasks;
    using Interfaces;
    using Microsoft.WindowsAzure.Storage.DataMovement;
    using Shared;

    public class BlobDataManager : IBlobDataManager
    {
        public ProgressRecorder ProgressRecorder { get; set; }

        public async Task UploadFileToBlobAsync(string sourceFilePath, BlobProperties properties)
        {
            Validate.BlobContainerName(properties.ContainerName, "containerName");
            Validate.BlobName(properties.BlobName, "blobName");

            var destinationBlob = DataManager.GetCloudBlob(properties);
            TransferManager.Configurations.ParallelOperations = 64;

            var context = new SingleTransferContext
            {
                ShouldOverwriteCallback = (path, destinationPath) => properties.OverwriteDestination
            };

            ProgressRecorder = new ProgressRecorder();
            context.ProgressHandler = ProgressRecorder;

            await TransferManager.UploadAsync(sourceFilePath, destinationBlob, null, context, CancellationToken.None);
        }
    }
}