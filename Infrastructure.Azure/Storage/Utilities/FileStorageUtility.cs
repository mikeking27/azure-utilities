﻿namespace Infrastructure.Azure.Utilities
{
    using System.IO;
    using Interfaces;
    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.File;
    using Shared;

    public class FileStorageUtility : IFileStorageUtility
    {
        private readonly CloudFileShare _cloudFileShare;

        public FileStorageUtility(string fileShareName, string storageConnectionString)
        {
            Validate.String(fileShareName, "fileShareName");
            Validate.String(storageConnectionString, "storageConnectionString");

            var cloudStorageAccount = CloudStorageAccount.Parse(storageConnectionString);

            var fileClient = cloudStorageAccount.CreateCloudFileClient();

            _cloudFileShare = fileClient.GetShareReference(fileShareName);
            _cloudFileShare.CreateIfNotExists();
        }

        public bool DirectoryExists(string directoryName)
        {
            Validate.String(directoryName, "directoryName");
            var rootDirectory = _cloudFileShare.GetRootDirectoryReference();
            var directoryToTest = rootDirectory.GetDirectoryReference(directoryName);

            return directoryToTest.Exists();
        }

        public bool FileExists(string directoryName, string fileName)
        {
            Validate.String(directoryName, "directoryName");
            Validate.String(fileName, "fileName");

            var rootDirectory = _cloudFileShare.GetRootDirectoryReference();
            var directoryToUpdate = rootDirectory.GetDirectoryReference(directoryName);
            var file = directoryToUpdate.GetFileReference(fileName);

            return file.Exists();
        }

        public void CreateDirectory(string directoryName)
        {
            Validate.String(directoryName, "directoryName");

            var rootDirectory = _cloudFileShare.GetRootDirectoryReference();
            var newDirectory = rootDirectory.GetDirectoryReference(directoryName);
            newDirectory.CreateIfNotExists();
        }

        public void DeleteDirectory(string directoryName)
        {
            Validate.String(directoryName, "directoryName");

            var rootDirectory = _cloudFileShare.GetRootDirectoryReference();
            var newDirectory = rootDirectory.GetDirectoryReference(directoryName);
            newDirectory.DeleteIfExists();
        }

        public void WriteTextToFile(string directoryName, string fileName, string content)
        {
            Validate.String(directoryName, "directoryName");
            Validate.String(fileName, "fileName");
            Validate.String(content, "content");

            var rootDirectory = _cloudFileShare.GetRootDirectoryReference();
            var directoryToUpdate = rootDirectory.GetDirectoryReference(directoryName);
            var file = directoryToUpdate.GetFileReference(fileName);
            if (!file.Exists())
                file.Create(long.MaxValue);

            file.UploadText(content);
        }

        public void WriteStreamToFile(string directoryName, string fileName, Stream content)
        {
            Validate.String(directoryName, "directoryName");
            Validate.String(fileName, "fileName");
            Validate.Null(content, "content");

            var rootDirectory = _cloudFileShare.GetRootDirectoryReference();
            var directoryToUpdate = rootDirectory.GetDirectoryReference(directoryName);
            var file = directoryToUpdate.GetFileReference(fileName);
            if (!file.Exists())
                file.Create(long.MaxValue);

            file.UploadFromStream(content);
        }

        public void UploadFile(string directoryName, string fileName, string sourceFilePath)
        {
            Validate.String(directoryName, "directoryName");
            Validate.String(fileName, "fileName");
            Validate.String(sourceFilePath, "sourceFilePath");

            var rootDirectory = _cloudFileShare.GetRootDirectoryReference();
            var directoryToUpdate = rootDirectory.GetDirectoryReference(directoryName);
            var file = directoryToUpdate.GetFileReference(fileName);
            if (!file.Exists())
                file.Create(long.MaxValue);

            file.UploadFromFile(sourceFilePath);
        }

        public void DeleteFile(string directoryName, string fileName)
        {
            Validate.String(directoryName, "directoryName");
            Validate.String(fileName, "fileName");

            var rootDirectory = _cloudFileShare.GetRootDirectoryReference();
            var directoryToUpdate = rootDirectory.GetDirectoryReference(directoryName);
            var file = directoryToUpdate.GetFileReference(fileName);

            file.DeleteIfExists();
        }
    }
}