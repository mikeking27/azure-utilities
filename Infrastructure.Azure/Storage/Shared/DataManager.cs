﻿namespace Infrastructure.Azure.Shared
{
    using System;
    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Blob;
    using Microsoft.WindowsAzure.Storage.File;

    public class DataManager
    {
        private static CloudStorageAccount _storageAccount;
        private static CloudBlobClient _blobClient;
        private static CloudFileClient _fileClient;

        /// <summary>
        ///     Get a CloudBlob instance with the specified name and type in the given container.
        /// </summary>
        /// <param name="properties">BlobProperties</param>
        /// <returns>A CloudBlob instance with the specified name and type in the given container.</returns>
        public static CloudBlob GetCloudBlob(BlobProperties properties)
        {
            var client = GetCloudBlobClient(properties.ConnectionString);
            var container = client.GetContainerReference(properties.ContainerName);
            container.CreateIfNotExists();

            CloudBlob cloudBlob;
            switch (properties.BlobType)
            {
                case BlobType.AppendBlob:
                    cloudBlob = container.GetAppendBlobReference(properties.BlobName);
                    break;
                case BlobType.BlockBlob:
                    cloudBlob = container.GetBlockBlobReference(properties.BlobName);
                    break;
                case BlobType.PageBlob:
                    cloudBlob = container.GetPageBlobReference(properties.BlobName);
                    break;
                default:
                    throw new ArgumentException($"Invalid blob type {properties.BlobName}");
            }

            if (properties.Public)
            {
                var permission = container.GetPermissions();

                permission.PublicAccess = BlobContainerPublicAccessType.Container;
                container.SetPermissions(permission);
            }

            return cloudBlob;
        }

        /// <summary>
        ///     Get a CloudFile instance with the specified name in the given share.
        /// </summary>
        /// <param name="connectionString">Connection string.</param>
        /// <param name="shareName">Share name.</param>
        /// <param name="fileName">File name.</param>
        /// <returns>A CloudFile instance with the specified name in the given share.</returns>
        public static CloudFile GetCloudFile(string connectionString, string shareName, string fileName)
        {
            var client = GetCloudFileClient(connectionString);
            var share = client.GetShareReference(shareName);
            share.CreateIfNotExists();

            var rootDirectory = share.GetRootDirectoryReference();
            return rootDirectory.GetFileReference(fileName);
        }

        private static CloudBlobClient GetCloudBlobClient(string connectionString)
        {
            return _blobClient ?? (_blobClient = GetStorageAccount(connectionString).CreateCloudBlobClient());
        }

        private static CloudFileClient GetCloudFileClient(string connectionString)
        {
            return _fileClient ?? (_fileClient = GetStorageAccount(connectionString).CreateCloudFileClient());
        }

        private static CloudStorageAccount GetStorageAccount(string connectionString)
        {
            return _storageAccount ?? (_storageAccount = CloudStorageAccount.Parse(connectionString));
        }
    }
}