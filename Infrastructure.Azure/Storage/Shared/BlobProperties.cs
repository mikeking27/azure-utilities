﻿namespace Infrastructure.Azure.Shared
{
    using Microsoft.WindowsAzure.Storage.Blob;

    public class BlobProperties
    {
        public string ConnectionString { get; set; }
        public string ContainerName { get; set; }
        public string BlobName { get; set; }
        public BlobType BlobType { get; set; }
        public bool OverwriteDestination { get; set; }
        public bool Public { get; set; }
    }
}